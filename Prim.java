package Schilda_Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class Prim {
    private int V;
    private int dist[];
    private boolean visited[];
    private int parent[];
    private List<List<Node>> inputgraph;


    public Prim(int V) {
        this.V = V;
        this.dist = new int[V];
        this.visited = new boolean[V];
        this.parent = new int[V];
        this.inputgraph = new ArrayList<>();
        for (int i = 0; i < V; i++) {
            inputgraph.add(new ArrayList<>());
        }
    }

    public void prim() {
        for (int i = 0; i < V; i++) {
            dist[i] = Integer.MAX_VALUE;
            visited[i] = false;
        }
        PriorityQueue<Node> queue = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node a, Node b) {
                return a.distance - b.distance;
            }
        });
        queue.add(new Node(0, 0));
        dist[0] = 0;
        parent[0] = -1;
        while (queue.isEmpty() == false) {
            int u = queue.poll().node;
            visited[u] = true;
            for (Node neighbor : inputgraph.get(u)) {
                int v = neighbor.node;
                int weight = neighbor.distance;
                if (!visited[v] && dist[v] > weight) {
                    dist[v] = weight;
                    parent[v] = u;
                    queue.add(new Node(v, dist[v]));
                }
            }
        }
        int[][] outputMatrix = new int[V - 1][3];
        int index = 0;
        for (int i = 1; i < V; i++) {
            outputMatrix[index][0] = parent[i];
            outputMatrix[index][1] = i;
            outputMatrix[index][2] = dist[i];
            index++;
        }

    }

        public void addEdge ( int u, int v, int w){
            inputgraph.get(u).add(new Node(v, w));
            inputgraph.get(v).add(new Node(u, w));
        }

        public void printStraße(){
            for (int i = 1; i < V; i++) {
                System.out.println("Edge: " + parent[i] + " - " + i + " : " + dist[i]);
            }
        }

    public static void main(String[] args) {
        Prim prim = new Prim(7);
        prim.addEdge(0, 1, 2);
        prim.addEdge(0, 2, 4);
        prim.addEdge(1, 3, 5);
        prim.addEdge(2, 4, 2);
        prim.addEdge(2, 5, 4);
        prim.addEdge(3, 5, 5);
        prim.addEdge(4, 6, 1);
        prim.addEdge(5, 6, 2);

        prim.prim();
        prim.printStraße();
    }

}