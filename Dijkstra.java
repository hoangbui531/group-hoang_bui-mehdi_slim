package Schilda_Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class Dijkstra {
    private int V;
    private int dist[];
    private boolean visited[];
    private List<List<Node>> graph;

    public Dijkstra(int V) {
        this.V = V;
        this.dist = new int[V];
        this.visited = new boolean[V];
        this.graph = new ArrayList<>();
        for (int i = 0; i < V; i++) {
            graph.add(new ArrayList<>());
        }
    }


    public void dijkstra(int source) {

        for (int i = 0; i < V; i++) {
            dist[i] = Integer.MAX_VALUE;
            visited[i] = false;
        }
        dist[source] = 0;
        PriorityQueue<Node> minHeap = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node a, Node b) {
                return a.distance - b.distance;
            }
        });
        minHeap.offer(new Node(source, 0));
        System.out.println("Die Reihenfolge, in der das Feuerwerk explodiert, wird sein: ");

        while (minHeap.isEmpty() == false) {
            int u = minHeap.poll().node;
            visited[u] = true;
            System.out.print(u +" ");
            for (Node v : graph.get(u)) {
                if (visited[v.node] == false && dist[u] != Integer.MAX_VALUE && dist[u] + v.distance < dist[v.node]) {
                    dist[v.node] = dist[u] + v.distance;
                    minHeap.offer(new Node(v.node, dist[v.node]));
                }
            }
        }
        System.out.println("");
        for (int i = 0; i < V; i++) {
            System.out.println("The shorted path from node " + source + " to " + i + " is " + dist[i]);
        }

    }


    public void addEdge(int u, int v, int w) {
        graph.get(u).add(new Node(v, w));
        graph.get(v).add(new Node(u, w));
    }
}

