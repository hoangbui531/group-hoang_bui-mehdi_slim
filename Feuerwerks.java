package Schilda_Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class Feuerwerks {
    public static void main(String[] args) {
        Dijkstra d = new Dijkstra(7);
        d.addEdge(0, 1, 2);
        d.addEdge(0, 2, 4);
        d.addEdge(1, 3, 5);
        d.addEdge(2, 4, 2);
        d.addEdge(2, 5, 4);
        d.addEdge(3, 5, 5);
        d.addEdge(4, 6, 1);
        d.addEdge(5, 6, 2);

        d.dijkstra(0);


    }
}
