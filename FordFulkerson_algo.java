package Schilda_Project;

import java.util.*;

public class FordFulkerson_algo {
    private int V;
    private int[] parent;
    private boolean[] visited;
    private List<List<NodeWasser>> graph;
    private List<List<NodeWasser>> residualGraph;


    public FordFulkerson_algo(int V) {
        this.V = V;
        this.parent = new int[V];
        this.visited = new boolean[V];
        this.graph = new ArrayList<>();
        for (int i = 0; i < V; i++) {
            graph.add(new ArrayList<>());
        }
    }

    public void addEdge(int start, int ziel, int kapazitat) {
        graph.get(start).add(new NodeWasser(ziel, kapazitat));
    }

    public boolean bfs(int source, int ziel) {
        Arrays.fill(parent, -1);
        Arrays.fill(visited, false);

        Queue<Integer> queue = new LinkedList<>();
        queue.add(source);
        visited[source] = true;

        while (!queue.isEmpty()) {
            int u = queue.poll();
            for (NodeWasser nachbar : graph.get(u)) {
                int adjacencyNode = nachbar.node;
                int adjacencyKapazitat = nachbar.kapacität;
                if (!visited[adjacencyNode] && adjacencyKapazitat > 0) {
                    visited[adjacencyNode] = true;
                    parent[adjacencyNode] = u;
                    queue.add(adjacencyNode);
                    if (adjacencyNode == ziel) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public int maxFlow(int source, int ziel) {
        int maxFlow = 0;
        while (bfs(source, ziel)) {
            int flow = Integer.MAX_VALUE;
            for (int node = ziel; node != source; node = parent[node]) {
                int parentNode = parent[node];
                for (NodeWasser nachbar : graph.get(parentNode)) {
                    if (nachbar.node == node) {
                        flow = Math.min(flow, nachbar.kapacität);
                    }
                }
            }
            for (int node = ziel; node != source; node = parent[node]) {
                int parentNode = parent[node];
                for (NodeWasser nachbar : graph.get(parentNode)) {
                    if (nachbar.node == node) {
                        nachbar.kapacität -= flow;
                    }
                }
                for (NodeWasser nachbar : graph.get(node)) {
                    if (nachbar.node == parentNode) {
                        nachbar.kapacität += flow;
                    }
                }
            }
            maxFlow += flow;
        }
        return maxFlow;
    }
    public void printRemainingNetwork() {
        for (int i = 0; i < V; i++) {
            for (NodeWasser nachbar : graph.get(i)) {
                if (nachbar.kapacität > 0) {
                    System.out.println("Cạnh từ " + i + " đến " + nachbar.node + " có kapacität " + nachbar.kapacität);
                }
            }
        }
    }
    public void createResidualGraph(){
        List<List<NodeWasser>> residualGraph = new ArrayList<>();
        for (int i = 0; i < V; i++) {
            residualGraph.add(new ArrayList<>());
        }
        //add forward edges to residualGraph
        for (int i = 0; i < V; i++) {
            for (NodeWasser nachbar : graph.get(i)) {
                residualGraph.get(i).add(new NodeWasser(nachbar.node, nachbar.kapacität));
            }
        }
        //add backward edges to residualGraph
        for (int i = 0; i < V; i++) {
            for (NodeWasser nachbar : graph.get(i)) {
                int adjacencyNode = nachbar.node;
                int adjacencyKapazitat = nachbar.kapacität;
                if(!checkEdgeExist(residualGraph, adjacencyNode, i)){
                    residualGraph.get(adjacencyNode).add(new NodeWasser(i, 0));
                }
            }
        }
    }
    private boolean checkEdgeExist(List<List<NodeWasser>> residualGraph, int start, int ziel) {
        for (NodeWasser nachbar : residualGraph.get(start)) {
            if (nachbar.node == ziel) {
                return true;
            }
        }
        return false;
    }



}

