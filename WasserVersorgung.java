package Schilda_Project;


public class WasserVersorgung {
    public static void main(String[] args) {
        int V = 4;
        FordFulkerson_algo ff = new FordFulkerson_algo(V);

       // Add edges to the graph
        ff.addEdge(0, 1, 16);
        ff.addEdge(0, 2, 13);
        ff.addEdge(1, 3, 12);
        ff.addEdge(2, 1, 4);
        ff.addEdge(2, 4, 11);
        ff.addEdge(3, 2, 9);
        ff.addEdge(3, 5, 20);
        ff.addEdge(4, 3, 7);
        ff.addEdge(4, 5, 4);


        // Find the maximum flow
        int maxFlow = ff.maxFlow(0, 3);
        ff.printRemainingNetwork();
        System.out.println("Maximum flow: " + maxFlow + " m^3/s");
        // Compare the maximum flow to the flow rate needed for the new supermarket
        int flowRateNeeded = 20; // in m^3/s
        if (maxFlow >= flowRateNeeded) {
            System.out.println("Das bestehende Netz kann " + maxFlow + " m^3/s Wasser liefern, was ausreicht, um den neuen Supermarkt mit Wasser zu versorgen.");
        }else{
            double additionalFlowRate = flowRateNeeded - maxFlow;
            System.out.println("Das bestehende Netz kann " + maxFlow + " m^3/s Wasser liefern,was nicht ausreicht, um den neuen Supermarkt mit Wasser zu versorgen.");
            System.out.println("Zusätzliche Durchflussrate von " + additionalFlowRate + " ist erforderlich.");
        }
    }
}
